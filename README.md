
# DMX Control Package

Das hier ist ein Paket von Zeug, mit dem du direkt anfangen kannst dein eigenes
kleines oder großes interaktives DMX-Lichtsteuerungs-Setup zu bauen.

Es handelt sich hierbei um eine Sammlung verschiedener Open-Source-Projekte, die
machinaeXphilip zusammengestellt hat für ein schönes Workshop Paket.

Ziel dieses Repos ist ein Komplettpaket für Workshops und kleine Lichtsteuerungen
für interessierte Laien bereitzustellen ohne von zu vielen Stellen einzelne Teile
und Module herunterladen zu müssen.

## video tutorial

ergänzend zu diesem readme kannst du dir auch den 5 minuten schnell durchlauf ansehen:

https://youtu.be/EC4cp05uzpA

## was du brauchst

installiert haben solltest du:

- node-js: https://nodejs.org/en/

- wenn du die Selbstbau Variante benutzt und nicht ein fertiges DMX Modul wie z.B. ENTTEC-DMX-USB-PRO:

  - Die Arduino IDE: https://www.arduino.cc

  - Fritzing: https://www.fritzing.org um die .fzz Datei anzugucken

Als Hardware brauchst du:

- ein Arduino (TM) kompatibles Board (wir haben ein UNO benutz aber andere tun es auch)

ODER

- ein DMX Modul dass unter https://github.com/machinaeXphilip/node-dmx#dmxregisterdrivername-module gelistet wird (Wir haben eine ENTTEC-DMX-USB-PRO MkII benutzt, aber mehr sind unter dem
  Link gelistet)

## Selbstbau

Wenn du dein eigenes DMX-Interface bauen willst:

Lade dir *OPEN DMX adapter.fz* herunter und öffne die Datei mit der Fritzing Software. Dort sind alle Bauteile gelistet. Das ganze kostet dich nur um die 3-4 EUR für den MAX485 chip und das Arduino oder welchen Mikrocontroller du auch immer benutzen willst.


## Benutzen

Erstmal lade dir das ganze Paket herunter.

Wenn du dein eigenes Arduino basiertes DMX-Device bauen willst folge zu erste dem Punkt *Selbstbau*


Öffne dein Terminal und navigiere in den heruntergeladenen Ordner.

$`cd DMX_paket`

Stecke dein DMX-Device deiner Wahl in deinen USB Port.

Danach musst du herausfinden welcher Port benutzt wird. Das geht je nach Betriebssystem unterschiedlich. (gut erklärt wird das unter: https://de.mathworks.com/help/supportpkg/arduinoio/ug/find-arduino-port-on-windows-mac-and-linux.html)
(Wenn du die Arduino-IDE offen hast, kannst du auch viel einfacher auf Tools > Ports klicken und gucken wie dein Gerät heisst)

Wenn du die Adresse deines Ports hast:

Ändere mit deinem Texteditor deiner Wahl in *dmx_control_server.js* Zeile 34 oder 35:

```
myDMX.addUniverse("one","HIER_MUSS_DIE_DEVICE_ID_STEHEN!","HIER MUSS DIE PORTADRESSE STEHEN!");
```

also wenn du zum Beispiel (auf MacOS oder linux zum Beispiel unser Gerät auf port "/dev/cu.usbmodem143231") entdeckt hast und ein Arduino mit SimpleDMX benutzen, trägst du folgendes ein:

```
myDMX.addUniverse("one","arduino-simple-dmx","/dev/cu.usbmodem143231");
```

Jetzt kannst du im Terminal den Server starten:

$`node dmx_control_server.js`

Wenn der läuft ohne Fehleranzeige, kannst du den Browser öffnen und http://localhost:8080 ansteuern. Juhu. Du kannst jetzt die Kanäle 1-4 an den angeschlossenen DMX-Devices steuern.

Wenn du mehr Kanäle willst oder ein anderes Interface:
Guck dir das HTML Zeug in dmx_control_server.js an und modifizier es. Vergiss nicht deine Mods zu teilen!

## Lizenzen, Copyrights und Geschichte

Das nodejs-Modul ist ursprünglich von https://github.com/wiedi/node-dmx
unter MIT License Copyright (c) 2012-2018 Sebastian Wiedenroth veröffentlicht.

(Dankeschön, lieber Sebastian! Ohne dich und alle Contributer wäre das ganze Projekt hier nicht möglich!)

und wurde von machinaeXphilip unter https://github.com/machinaeXphilip/node-dmx
modifiziert (genauer genommen hat er nur einen improvisierten Arduino-Treiber hinzugefügt) um auch mit dem hier beschriebenen Arduino-DIY DMX Controller benutzt werden zu können.

Der o.g. Arduino (TM) Treiber funktioniert mit dem modifizierten Beispiel "SerialtoDMX" aus der Arduino (TM) Bibliothek https://github.com/TinkerKit/DmxMaster. Die modifikation ist ausschließlich eine höherere Baudrate. Lizenz ist nicht zu finden.

(Vielen Dank an das TinkerKit Team! Ohne euch könnten könnten wir usLEDsupply's layout nur benutzen wenn wir wüssten wie das DMX Protocoll wirklich funktioniert und was wir ans Serial schicken müssten. Damit sind wir erstmal gescheitert und waren sehr froh eure Bibliothek und das tolle Beispiel zu finden!)

Das funktioniert mit dem TinkerKit (TM) DMX-Shield, aber (für Workshops relevant)
auch mit der im Fritzing File beschriebenen DIY-Aufbau mit einem Arduino.

Das Fritzing Layout für den Arduino_DMX controller ist von http://fritzing.org/projects/arduino-to-dmx-converter/ und lizensiert
(CC BY-SA 3.0) von Fritzing User usLEDsupply: http://fritzing.org/profiles/usLEDsupply/

(Tausend Dank, usLEDsupply! Ohne dich müssten wir unseren Workshopees sagen dass sie USB-DMX-Adapter kaufen müssen! So können wir mehrere Optionen anbieten!)
