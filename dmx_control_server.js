// html interfaced dmx control via node using: https://github.com/wiedi/node-dmx
// proof of concept for later interfaces and modules for plaiframe
// variant of the  for use with Arduino + DMXMaster libraries SerialtoDMX Example:
//  https://github.com/machinaeXphilip/node-dmx
// licence: ccnc by Philip Steimel 2018

/*
  this version is in particular for workshop purposes. It creates an html
interface that reacts to dragging of the displayed sliders as well as to
keypresses to keys Q,A (channel 1) W,S (Ch 2), E,D (Ch 3) and R,F (Ch 4) to
control the DMX output.

To adapt to your computer and DMX device:
1.) check at https://github.com/machinaeXphilip/node-dmx if your device is supported
2.) if yes: add in line 31 (myDMX.addUniverse etc) as second argument the name
    of your device as specified on github
3.) add as third argument the USB-Ports Name your device is currently connected
    to.
    That differs from MacOs, Win and Linux machines:
      to learn how to find the name of your USB port on your system, see:
      https://de.mathworks.com/help/supportpkg/arduinoio/ug/find-arduino-port-on-windows-mac-and-linux.html

  to add more channel sliders, just add input range to html body and change id
to "dmx"+ channelnumber
*/

// DMX init:
const express = require('express');
//const DMX = require("dmx"); // if using original module
const DMX = require("./dmx_withArduino") // if using the modified module

let myDMX = new DMX();

myDMX.addUniverse("one","enttec-usb-dmx-pro","/dev/cu.usbserial-ENXE6UVB"); // <--- here if ENTTEC-USB-DMX-PRO Box
//myDMX.addUniverse("one","arduino-simple-dmx","/dev/cu.usbmodem143231"); // <--- here if DIY Arduino with SimpleDMX

myDMX.update("one", {1:0,2:0,3:0,4:0} ); // initializes black on channels 1-4

//////// webserver stuff ///////////

// get local IP in node js:

const ifaces = require('os').networkInterfaces();
let address;

  Object.keys(ifaces).forEach(dev => {
    ifaces[dev].filter(details => {
      if (details.family === 'IPv4' && details.internal === false) {
        address = details.address;
      }
    });
  })

// make webserver and serve a html interface that talks with this app again.

var app = express();

app.get('/', function (req, res) {
  res.send(`
    <html>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style>
    input,label{width: 90%; margin-left: 5%}
    .slider { -webkit-appearance:none;width:90%;height:15px; border-radius:5px;
    background:#d3d3d3;outline:none;opacity:0.7;-webkit-transition:.2s;
    transition:opacity .2s; }
    .slider::-webkit-slider-thumb {-webkit-appearance: none;appearance: none;
    width:25px;height:25px;border-radius:50%;background:#4CAF50;cursor:pointer;}
    .slider::-moz-range-thumb {width: 25px;height: 25px;border-radius: 50%;
    background: #4CAF50;cursor: pointer;}
    </style>
    <body>

<br><br>

    <label for="dmx1">1</label>
    <input type="range" id="dmx1" name="dmx1"
         min="0" max="255" value="0" class="slider" />

<br><br>

    <label for="dmx2">2</label>
    <input type="range" id="dmx2" name="dmx2"
        min="0" max="255" value="0" class="slider" />

<br><br>

    <label for="dmx3">3</label>
    <input type="range" id="dmx3" name="dmx3"
        min="0" max="255" value="0" class="slider" />

<br><br>

    <label for="dmx4">4</label>
    <input type="range" id="dmx4" name="dmx4"
        min="0" max="255" value="0" class="slider" />

    </body>

    <script>


      let allsliders = $(".slider")
      allsliders.on("input",function(elem){
        let channel = elem.target.id.replace("dmx","");
        console.log(channel + " : " + elem.target.value);

        let xhttp = new XMLHttpRequest();
        xhttp.open("GET", "http://`+address+`:8080/dmx/"+channel+"/"+elem.target.value, true);
        xhttp.send();
      })


      $("body").keypress(keyListener);

      function keyListener(event)
      {
        //console.log(event);
        if (event.key == "q")
        {
          increase(1);
        }
        if (event.key == "w")
        {
          increase(2);
        }
        if (event.key == "e")
        {
          increase(3);
        }
        if (event.key == "r")
        {
          increase(4);
        }

        if (event.key == "a")
        {
          decrease(1);
        }
        if (event.key == "s")
        {
          decrease(2);
        }
        if (event.key == "d")
        {
          decrease(3);
        }
        if (event.key == "f")
        {
          decrease(4);
        }

      }

      function increase(ch)
      {
        let val = parseInt($("#dmx"+ch).val());
        val = val + 20;
        if (val > 255) {val = 255}
        console.log(val);
        sendDMX(ch,val)
        $("#dmx"+ch).val(val);
      }

      function decrease(ch)
      {
        let val = parseInt($("#dmx"+ch).val());
        val = val - 20;
        if (val < 0) {val = 0}
        console.log(val);
        sendDMX(ch,val)
        $("#dmx"+ch).val(val);
      }

      function sendDMX(channel, value)
      {
        let xhttp = new XMLHttpRequest();
        xhttp.open("GET", "http://`+address+`:8080/dmx/"+channel+"/"+value, true);
        xhttp.send();
      }

    </script>

    </html>
    `);
});

app.get('/dmx/*', function (req, res) {
  //console.log(req.params);
  let channel = req.params[0].split("/")[0];
  let value = req.params[0].split("/")[1];
  let newlight = {};
  newlight[channel]= value;
  console.log(newlight);

  myDMX.update("one", newlight );

  res.send('ok');
});

app.listen(8080, function () {
  console.log('Example app listening on port 8080!');
});
